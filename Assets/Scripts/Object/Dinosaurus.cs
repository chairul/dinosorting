﻿using UnityEngine;

public class Dinosaurus {

	public string Name{ get; set; }
	public string Size;

	public Dinosaurus(string dinoName, string size){
		this.Name = dinoName;
		this.Size = size;
	}
}