﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class Sorting_Controller : MonoBehaviour {

	public static Sorting_Controller instance;

	[SerializeField] Sprite infoCardTrue, infoCardFalse;
	[SerializeField] Sprite[] dinoStageLight;
	[SerializeField] GameObject[] dinoStage;
	[SerializeField] GameObject[] dinoStageInfocard;
	[SerializeField] GameObject dinoPlace;

	JSONNode JSONLoaded;
	List<string> dinoL=new List<string> (), dinoM=new List<string> (), dinoS=new List<string> ();
	GameObject[] instantiated = new GameObject[4];
	List<string> chosenDino = new List<string> ();
	List<int> loop = new List<int>{0,1,2,3};
	GameObject currentDinoGO;

	[HideInInspector] public string currentDino;
	int dinoSorted = 0, countTrue = 0, countFalse = 0;

	void Awake() {
		instance = this;
	}

	void Start () {
		LoadJSON ("Data/sorting");
	}

	void LoadJSON(string url){
		string json = Resources.Load<TextAsset>(url).text;
		OnLoadJSON (json);
	}

	void OnLoadJSON(string json){
		JSONLoaded = JSON.Parse(json);

		for (int l = 0; l < JSONLoaded ["Sorting"] ["dinos"] ["large"].Count; l++) {
			dinoL.Add (JSONLoaded ["Sorting"] ["dinos"] ["large"] [l] ["name"]);
		}
		for (int m = 0; m < JSONLoaded ["Sorting"] ["dinos"] ["medium"].Count; m++) {
			dinoM.Add (JSONLoaded ["Sorting"] ["dinos"] ["medium"] [m] ["name"]);
		}
		for (int s = 0; s < JSONLoaded ["Sorting"] ["dinos"] ["small"].Count; s++) {
			dinoS.Add (JSONLoaded ["Sorting"] ["dinos"] ["small"] [s] ["name"]);
		}

		ShuffleDinoToSort ();
		SetContentLayout ();
	}

	void ShuffleDinoToSort(){
		for (int i = 0; i < loop.Count; i++) {
			int temp = loop [i];
			int randomIndex = Random.Range (i, loop.Count);
			loop [i] = loop [randomIndex];
			loop [randomIndex] = temp;
		}
	}

	string GetDinoSize(string dinoName){
		if (dinoL.Contains (dinoName))
			return "large";
		else if (dinoM.Contains (dinoName))
			return "medium";
		else if (dinoS.Contains (dinoName))
			return "small";
		else
			return "";
	}

	int GetDinoIndex(string dinoName){
		if (GetDinoSize (dinoName).Equals ("large"))
			return dinoL.FindIndex (x => dinoName.Equals (x));
		else if (GetDinoSize (dinoName).Equals ("medium"))
			return dinoM.FindIndex (x => dinoName.Equals (x));
		else if (GetDinoSize (dinoName).Equals ("small"))
			return dinoS.FindIndex (x => dinoName.Equals (x));
		else
			return 0;
	}

	void SetContentLayout(){
		int indexL1 = Random.Range (0, dinoL.Count - 1);
		int indexL2 = indexL1++;
		if (indexL1 == dinoL.Count - 1)
			indexL2 -= 2;

		chosenDino.Add (dinoL [indexL1]);
		chosenDino.Add (dinoL [indexL2]);
		chosenDino.Add (dinoM [Random.Range (0, dinoM.Count)]);
		chosenDino.Add (dinoS [Random.Range (0, dinoS.Count)]);

		for (int i = 0; i < chosenDino.Count; i++) {
			PutDinoToStage (i);
		}

		PutSortingDino (loop[dinoSorted]);
	}

	void PutDinoToStage(int index){
		string size = GetDinoSize (chosenDino [index]);

		instantiated [index] = Instantiate (Resources.Load<GameObject> ("Prefabs/DinoSorting/dino_sorting_" + chosenDino [index]), dinoStage [index].transform);
		instantiated [index].AddComponent<Sorting_Item> ().Create (chosenDino [index], size);
	}

	void PutSortingDino(int index){
		ClearPreviousDino ();

		currentDino = chosenDino [index];
		JSONNode curNode = JSONLoaded ["Sorting"] ["dinos"] [GetDinoSize (chosenDino [index])] [GetDinoIndex (chosenDino [index])];

		Vector3 dinoPos = new Vector3 ( curNode ["position"] ["x"].AsFloat, curNode ["position"] ["y"].AsFloat);
		Vector3 dinoScale = new Vector3 (curNode ["scale"] ["x"].AsFloat, curNode ["scale"] ["y"].AsFloat);

		currentDinoGO = Instantiate (Resources.Load<GameObject> ("Prefabs/DinoSorting/dino_sorting_" + chosenDino [index]), dinoPlace.transform);
		currentDinoGO.GetComponent<RectTransform> ().anchoredPosition = dinoPos;
	}

	void ClearPreviousDino(){
		if (currentDino != null) {
			Destroy (currentDinoGO);
		}
	}

	void RevealDino(int index){
		instantiated[index].GetComponent<Mask> ().enabled = false;
		instantiated[index].transform.GetChild (0).gameObject.SetActive (false);
		instantiated [index].GetComponent<Sorting_Item> ().isActive = false;
	}

	public void NotifyAnswer(bool answer, Sorting_Item dino){
		Image infocard = dinoStageInfocard [loop[dinoSorted]].GetComponent<Image> ();
		infocard.enabled = true;

		dinoStage [loop [dinoSorted]].GetComponent<Image> ().sprite = dinoStageLight [loop [dinoSorted]];

		if (answer) {
			countTrue++;
			infocard.sprite = infoCardTrue;
		} else {
			countFalse++;
			infocard.sprite = infoCardFalse;
		}

		RevealDino (loop[dinoSorted]);

		dinoSorted++;
		if (dinoSorted < chosenDino.Count) {
			PutSortingDino (loop[dinoSorted]);
		} else {
			FinishGame ();
		}
	}

	void FinishGame(){
		Debug.Log ("hooray");
	}
}
