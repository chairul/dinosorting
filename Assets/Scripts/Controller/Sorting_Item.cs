﻿using UnityEngine;
using UnityEngine.UI;

public class Sorting_Item : MonoBehaviour {
	
	public Dinosaurus dino;
	public bool isActive = true;

	public void Create(string name, string size) {
		dino = new Dinosaurus (name, size);
		Setup ();
	}

	void Setup(){
		Button button = gameObject.AddComponent<Button> ();
		button.onClick.AddListener (delegate {
			ClickAnswer ();
		});
		button.transition = Selectable.Transition.None;

		gameObject.AddComponent<Mask> ();

		GameObject cover = transform.GetChild (0).gameObject;
		cover.SetActive (true);
	}

	void ClickAnswer(){
		if (isActive) {
			if (dino.Name.Equals (Sorting_Controller.instance.currentDino)) {
				Sorting_Controller.instance.NotifyAnswer (true, this);
			} else {
				Sorting_Controller.instance.NotifyAnswer (false, this);
			}
		}
	}
}